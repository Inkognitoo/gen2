<?php

/**
 * Created by PhpStorm.
 * User: Inkognitoo
 * Date: 18.12.2014
 * Time: 16:34
 */

require_once C.'Gen.php';

class Algorithm
{
    public $x = [];
    public $y = [];

    public $gens = [];

    public function __construct()
    {
        for($i = 0; $i < GEN_COUNT; $i++){
            $this->gens[] = new Gen();
        }

        for($i = 0; $i < 10; $i++){
            $this->x[] = $i;
            $this->y[] = $i * $i;
        }
    }

    public function Start()
    {
        for ($j = 0; $j < AGE_COUNT; $j++) {

            //поколение пошло
            $this->Selection();//сортируем по выживаемости
            //спариваем половину сильнейших
            for ($i = 0; $i <= round(GEN_COUNT / 4); $i += 2) {
                //два родителя дают четырёх потомков
                for ($k = 0; $k < 4; $k++) {
                    $this->gens[] = $this->Pairing($this->gens[$i], $this->gens[$i + 1]);
                }
            }

            $this->Selection();//сортируем по выживаемости
            if (count($this->gens) > GEN_COUNT) {
                $this->gens = array_slice($this->gens, 0, GEN_COUNT); //убиваем слабейших
            }
        }

        return $this->gens[0];
    }

    private function Pairing($mom, $dad)
    {
        $son_chromosome = $dad->chromosome;
        for($i = 0; $i < 11; $i++){
            if( mt_rand(0,1) === 1) {
                $son_chromosome[$i] = $mom->chromosome[$i];
            }
        }

        //шанс мутации - 1/100
        if( mt_rand(0,MUTATION) === 1) {
            $n = mt_rand(0, 11);
            $son_chromosome[$n] = mt_rand(0, 7);
        }

        return new Gen($son_chromosome);
    }

    private function Selection()
    {
        $foo = [];
        for($i = 0; $i < GEN_COUNT; $i++){
            $result = 0;

            $n = count($this->x);
            for($j = 0; $j < $n; $j++){
                $result += abs( $this->gens[$i]->GetNumber($this->x[$j]) - $this->y[$j] );
            }

            $foo[] = $result;
        }
        if(count($foo) < count($this->gens))
            $foo = array_pad ($foo, count($this->gens), 0);

        array_multisort($foo, $this->gens); //выбираем сильнейших особей
    }

}
?>
