<?php
/**
 * Created by PhpStorm.
 * User: Inkognitoo
 * Date: 25.12.2014
 * Time: 2:43
 */

class Node
{
    public $sign;
    public $next;

    public function __construct(&$chromosome)
    {
        $this->sign = $this->foo[$chromosome[0]];

        if($this->sign != 'x') {
            $n = $this->arity[$this->foo[$chromosome[0]]];

            for ($i = 0; $i < $n; $i++) {

                if (count($chromosome) > 0)
                    array_shift($chromosome);   //удаляем первый элемент

                if (count($chromosome) > 0) {
                    $this->next[$i] = new Node($chromosome);
                } else {                        //в любой непонятной ситуации - пихай иксы
                    $bar = [6];
                    $this->next[$i] = new Node($bar);
                }
            }
        }
    }

    public function toString()
    {
        $string = '';
        switch ($this->sign) {
            case "+":
                $string .= '(' . $this->next[0]->toString() . ') + (' . $this->next[1]->toString() . ')';
                break;
            case "-":
                $string .= '(' . $this->next[0]->toString() . ') - (' . $this->next[1]->toString() . ')';
                break;
            case "*":
                $string .= '(' . $this->next[0]->toString() . ') * (' . $this->next[1]->toString() . ')';
                break;
            case "/":
                $string .= '(' . $this->next[0]->toString() . ') / (' . $this->next[1]->toString() . ')';
                break;
            case "sin":
                $string .= 'sin(' . $this->next[0]->toString() . ')';
                break;
            case "cos":
                $string .= 'cos(' . $this->next[0]->toString() . ')';
                break;
            case "x":
                $string .= 'x';
                break;
            case "sqr":
                $string .= 'sqr(' . $this->next[0]->toString() . ')';
                break;
        }
        return $string;
    }

    public function Calculate($x)
    {
        $result = 0;
        switch ($this->sign) {
            case "+":
                $result = $this->next[0]->Calculate($x) + $this->next[1]->Calculate($x);
                break;
            case "-":
                $result = $this->next[0]->Calculate($x) - $this->next[1]->Calculate($x);
                break;
            case "*":
                $result = $this->next[0]->Calculate($x) * $this->next[1]->Calculate($x);
                break;
            case "/":
                if($this->next[1]->Calculate($x) != 0)
                    $result = $this->next[0]->Calculate($x) / $this->next[1]->Calculate($x);
                else
                    $result = $this->next[0]->Calculate($x) / 1;
                break;
            case "sin":
                $result = sin($this->next[0]->Calculate($x));
                break;
            case "cos":
                $result = cos($this->next[0]->Calculate($x));
                break;
            case "x":
                $result = $x;
                break;
            case "sqr":
                $result = $this->next[0]->Calculate($x) * $this->next[0]->Calculate($x);
                break;
        }
        return $result;
    }

    private $arity = [
        '+' => 2,
        '-' => 2,
        '*' => 2,
        '/' => 2,
        'sin' => 1,
        'cos' => 1,
        'x' => 1,
        'sqr' => 1,
    ];

    private $foo = [
        0 => '+',
        1 => '-',
        2 => '*',
        3 => '/',
        4 => 'sin',
        5 => 'cos',
        6 => 'x',
        7 => 'sqr',
    ];
}

?>