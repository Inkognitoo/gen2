<?php
/**
 * Created by PhpStorm.
 * User: Inkognitoo
 * Date: 18.12.2014
 * Time: 16:23
 */
require_once C.'Tree.php';

class Gen {
    public $chromosome = [];
    private $tree;

    public function __construct($newChromosome = false)
    {
        if($newChromosome !== false){
            $this->chromosome = $newChromosome;
        }else{
            for($i = 0; $i < 12; $i++)
            {
                $this->chromosome[] = mt_rand(1, 7);
            }
        }

        $this->CreateTree();
    }

    public function GetNumber($x)
    {
        return $this->tree->Calculate($x);
    }

    public function toString()
    {
        return $this->tree->toString();
    }


    private function CreateTree()
    {
        $this->tree = new Tree($this->chromosome);
    }
}

?>