<?php
/**
 * Created by PhpStorm.
 * User: Inkognitoo
 * Date: 24.12.2014
 * Time: 22:09
 */
require_once C.'Node.php';

class Tree
{
    public $node;

    public function __construct($chromosome)
    {
        $this->node = new Node($chromosome);
    }

    public function toString()
    {
        return $this->node->toString();
    }

    public function Calculate($x)
    {
        return $this->node->Calculate($x);
    }
}

?>