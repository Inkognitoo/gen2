<?php
require_once 'kernel.conf.php';
require_once C.'Algorithm.php';

$foo = new Algorithm();

$bar = $foo->Start();

$json['status'] = true;
$json['f'] = $bar->toString();

echo json_encode($json);