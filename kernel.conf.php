<?php
/**
 * Created by PhpStorm.
 * User: Павел
 * Date: 12.09.14
 * Time: 14:27
 */

define ('HD', dirname(__FILE__).'/');
define ('C',  HD.'kernel/class/');
define ('TEMPLATE',  HD.'kernel/template/');
define ('VENDOR',  HD.'kernel/vendor/');

define ('CHROMOSOME_COUNT', 12);
define ('GEN_COUNT', 20);

define ('AGE_COUNT', 1000);
define ('MUTATION', 10);

set_time_limit(0); //время выполнения неограничено